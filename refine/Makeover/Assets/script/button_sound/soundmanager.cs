using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class soundmanager : MonoBehaviour
{
    public AudioSource buttonclick_character;
    public AudioSource typemessage_sound;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void buttonclock_sound_btn(){
        buttonclick_character.Play();
    }
    public void typemessagebtn(){
        typemessage_sound.Play();
    }
}
