using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Kino;
public class faceexpression_final : MonoBehaviour
{
    public Slider slider;
     public GameObject faceexpression_01;
    public GameObject faceexpression_02;
    public GameObject faceexpression_03;
    public GameObject faceexpresssion_04;
    public GameObject faceexpression_05;
    public GameObject faceexpression_hapy_01;
      public GameObject faceexpression_hapy_02;
    public AnalogGlitch analogglitchfunction;
    public float ColorDrift;
    public float ScanLineJitter;
    public DigitalGlitch digitalglitchfunction;
    public float Intensity;
    public float VerticalJump;
    public float HorizontalShake;
    public GameObject popupmessage;
    public AudioSource glitch_sound;
public AudioSource normalbg;
public AudioSource sadbg;
public Animator healthlow;
public Animator normal;
public GameObject normalface;
public Animator happy;




//hastag
public GameObject hastag;
public GameObject hastag_01;
public GameObject hastag_02;
public GameObject hastag_03;
//hastag
public typewriter_effect typewriter_script;
public AudioSource pop_up_sound;
public GameObject lock_ui;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void healthbardecrease_player(){
         
        if(slider.value == 30){
            faceexpression_01.SetActive(false);
           faceexpression_02.SetActive(true);
            faceexpression_hapy_02.SetActive(false);
            faceexpression_03.SetActive(false);
             faceexpresssion_04.SetActive(false);
             faceexpression_05.SetActive(false);
                  faceexpression_hapy_01.SetActive(false);
                      normalface.SetActive(false);
                       hastag.SetActive(false);
              hastag_01.SetActive(true);
              hastag_02.SetActive(false);
              hastag_03.SetActive(false);
                  normal.Play("Yellow");
             Debug.Log("it works on this script");

        }
        if(slider.value == 20){
              faceexpression_02.SetActive(false);
                   faceexpression_01.SetActive(false);
            faceexpression_03.SetActive(true);
             faceexpresssion_04.SetActive(false);
             normalface.SetActive(false);
              faceexpression_hapy_02.SetActive(false);
             faceexpression_05.SetActive(false);
                  faceexpression_hapy_01.SetActive(false);
             analogglitchfunction.scanLineJitter = .108f;
               hastag.SetActive(false);
              hastag_01.SetActive(false);
              hastag_02.SetActive(true);
              hastag_03.SetActive(false);
             healthlow.Play("red");
        }
        if(slider.value == 10){
             faceexpresssion_04.SetActive(true);
          faceexpression_01.SetActive(false);
           faceexpression_hapy_02.SetActive(false);
            faceexpression_02.SetActive(false);
             faceexpression_03.SetActive(false);
             faceexpression_05.SetActive(false);
             hastag.SetActive(false);
              hastag_01.SetActive(false);
              hastag_02.SetActive(false);
              hastag_03.SetActive(true);
              faceexpression_hapy_01.SetActive(false);
              normalface.SetActive(false);
     
              analogglitchfunction.scanLineJitter = .299f;
              analogglitchfunction.colorDrift = .017f;
               hastag.SetActive(false);
              hastag_01.SetActive(false);
              hastag_02.SetActive(false);
              hastag_03.SetActive(true);
        }
        if(slider.value == 0){
            StartCoroutine(waitToShowFBScene5());
              IEnumerator waitToShowFBScene5(){
	           glitch_sound.Play();
              normalbg.Stop();
            digitalglitchfunction.intensity = .265f;
            analogglitchfunction.verticalJump = .282f;
             analogglitchfunction.horizontalShake = .109f;
             yield return new WaitForSeconds (1);
                  faceexpresssion_04.SetActive(false);
                  normalface.SetActive(false);
          faceexpression_01.SetActive(false);
           faceexpression_hapy_02.SetActive(false);
            faceexpression_02.SetActive(false);
                 faceexpression_hapy_01.SetActive(false);
             faceexpression_03.SetActive(false);
            
             
            yield return new WaitForSeconds (2);
            digitalglitchfunction.intensity = 0;
               analogglitchfunction.scanLineJitter = 0;
              analogglitchfunction.colorDrift = 0;
              analogglitchfunction.verticalJump = 0;
              analogglitchfunction.horizontalShake = 0;
              faceexpression_05.SetActive(true);
              normalface.SetActive(false);
              glitch_sound.Stop();
              sadbg.Play();
               yield return new WaitForSeconds (1);
              popupmessage.SetActive(true);
              lock_ui.SetActive(true);
              //place your audio here
              pop_up_sound.Play();
           
              }
        }
         if(slider.value == 60){
              faceexpression_hapy_01.SetActive(true);
              faceexpresssion_04.SetActive(false);
          faceexpression_01.SetActive(false);
          normalface.SetActive(false);
             analogglitchfunction.scanLineJitter = 0f;
              analogglitchfunction.colorDrift = 0f;
            faceexpression_02.SetActive(false);
             faceexpression_03.SetActive(false);
             faceexpression_05.SetActive(false);
             faceexpression_hapy_02.SetActive(false);
             happy.Play("green");
          }
           if(slider.value == 40){
               normal.Play("Yellow");
              Debug.Log("activeface");
              normalface.SetActive(true);
              hastag.SetActive(true);
              hastag_01.SetActive(false);
              hastag_02.SetActive(false);
                 analogglitchfunction.scanLineJitter = 0f;
              analogglitchfunction.colorDrift = 0f;
              hastag_03.SetActive(false);
              typewriter_script.hastagbtn();
              
          }

          if(slider.value == 80){
              faceexpression_hapy_01.SetActive(false);
               faceexpression_hapy_02.SetActive(true);
              faceexpresssion_04.SetActive(false);
          faceexpression_01.SetActive(false);
          normalface.SetActive(false);
          happy.Play("green");
             analogglitchfunction.scanLineJitter = 0f;
              analogglitchfunction.colorDrift = 0f;
            faceexpression_02.SetActive(false);
             faceexpression_03.SetActive(false);
             faceexpression_05.SetActive(false);
          }


        ///try this code
    }
    public void healthbar_increase(){
          if(slider.value == 60){
              faceexpression_hapy_01.SetActive(true);
              faceexpresssion_04.SetActive(false);
          faceexpression_01.SetActive(false);
          normalface.SetActive(false);
          happy.Play("green");
             analogglitchfunction.scanLineJitter = 0f;
              analogglitchfunction.colorDrift = 0f;
            faceexpression_02.SetActive(false);
             faceexpression_03.SetActive(false);
             faceexpression_05.SetActive(false);
             faceexpression_hapy_02.SetActive(false);
          }
           if(slider.value == 40){
               normal.Play("Yellow");
              Debug.Log("activeface");
                analogglitchfunction.scanLineJitter = 0f;
              analogglitchfunction.colorDrift = 0f;
              normalface.SetActive(true);
              
          }

          if(slider.value == 80){
              faceexpression_hapy_01.SetActive(false);
               faceexpression_hapy_02.SetActive(true);
              faceexpresssion_04.SetActive(false);
          faceexpression_01.SetActive(false);
          
          normalface.SetActive(false);
            faceexpression_02.SetActive(false);
             faceexpression_03.SetActive(false);
             faceexpression_05.SetActive(false);
          }
          if(slider.value == 40){
               normal.Play("Yellow");
              Debug.Log("activeface");
              normalface.SetActive(true);
              hastag.SetActive(true);
              hastag_01.SetActive(false);
              hastag_02.SetActive(false);
              hastag_03.SetActive(false);
          }
         if(slider.value == 30){
            faceexpression_01.SetActive(false);
           faceexpression_02.SetActive(true);
            faceexpression_hapy_02.SetActive(false);
            faceexpression_03.SetActive(false);
             faceexpresssion_04.SetActive(false);
             faceexpression_05.SetActive(false);
                  faceexpression_hapy_01.SetActive(false);
                      normalface.SetActive(false);
                       hastag.SetActive(false);
              hastag_01.SetActive(true);
              hastag_02.SetActive(false);
              hastag_03.SetActive(false);
                  normal.Play("Yellow");
             Debug.Log("it works on this script");

        }
        if(slider.value == 20){
              faceexpression_02.SetActive(false);
                   faceexpression_01.SetActive(false);
            faceexpression_03.SetActive(true);
             faceexpresssion_04.SetActive(false);
             normalface.SetActive(false);
              faceexpression_hapy_02.SetActive(false);
             faceexpression_05.SetActive(false);
                  faceexpression_hapy_01.SetActive(false);
             analogglitchfunction.scanLineJitter = .108f;
               hastag.SetActive(false);
              hastag_01.SetActive(false);
              hastag_02.SetActive(true);
              hastag_03.SetActive(false);
             healthlow.Play("red");
        }
          if(slider.value == 10){
             faceexpresssion_04.SetActive(true);
          faceexpression_01.SetActive(false);
           faceexpression_hapy_02.SetActive(false);
            faceexpression_02.SetActive(false);
             faceexpression_03.SetActive(false);
             faceexpression_05.SetActive(false);
             hastag.SetActive(false);
              hastag_01.SetActive(false);
              hastag_02.SetActive(false);
              hastag_03.SetActive(true);
              faceexpression_hapy_01.SetActive(false);
              normalface.SetActive(false);
     
              analogglitchfunction.scanLineJitter = .299f;
              analogglitchfunction.colorDrift = .017f;
               hastag.SetActive(false);
              hastag_01.SetActive(false);
              hastag_02.SetActive(false);
              hastag_03.SetActive(true);
        }
    }
}
