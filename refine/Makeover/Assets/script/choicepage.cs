using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class choicepage : MonoBehaviour
{
    public GameObject gamepage;
    public GameObject choicepage_pg;

    public GameObject avacado_avatar;
    public GameObject love_character;
    public GameObject ballon_character;
    public GameObject leaf_character;
    public GameObject lock_function;
    public GameObject nextpae;

    //// for your choice pg
    public GameObject avacado_choice;
    public GameObject leaf_choice;
    public GameObject love_choice;
    public GameObject Ballon_choice;
    public GameObject removehide;


    public GameObject no_friends_found;
    public GameObject friends_found;


    ///no friends function
    public GameObject balloon_character_img;
    public GameObject love_character_img;
    public GameObject leaf_character_img;
    public GameObject avacado_character_img;
    public GameObject okfucntion;
   public AudioSource notification;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void btnop(){
        gamepage.SetActive(false);
        choicepage_pg.SetActive(true);
    }

    ///charatcer selection
    public void avacado_btn(){
        avacado_avatar.SetActive(true);
        love_character.SetActive(false);
         ballon_character.SetActive(false);
         leaf_character.SetActive(false);

         avacado_choice.SetActive(true);
         leaf_choice.SetActive(false);
         Ballon_choice.SetActive(false);
         love_choice.SetActive(false);

         //no friends function
         avacado_character_img.SetActive(true);
         leaf_character_img.SetActive(false);
         balloon_character_img.SetActive(false);
         love_character_img.SetActive(false);

         
    }
     public void love_btn(){
        love_character.SetActive(true);
        avacado_avatar.SetActive(false);
        ballon_character.SetActive(false);
        leaf_character.SetActive(false);

        love_choice.SetActive(true);
        Ballon_choice.SetActive(false);
        leaf_choice.SetActive(false);
        avacado_choice.SetActive(false);

              //no friends function
              love_character_img.SetActive(true);
                leaf_character_img.SetActive(false);
                 balloon_character_img.SetActive(false);
                 avacado_character_img.SetActive(false);
    }
     public void ballon_btn(){
        ballon_character.SetActive(true);
        love_character.SetActive(false);
         avacado_avatar.SetActive(false);
         leaf_character.SetActive(false);

         Ballon_choice.SetActive(true);
         love_choice.SetActive(false);
         leaf_choice.SetActive(false);
         avacado_choice.SetActive(false);

               //no friends function
               balloon_character_img.SetActive(true);
               love_character_img.SetActive(false);
                avacado_character_img.SetActive(false);
                 leaf_character_img.SetActive(false);
    }
     public void leaf_btn(){
        leaf_character.SetActive(true);
        ballon_character.SetActive(false);
        love_character.SetActive(false);
        avacado_avatar.SetActive(false);

        leaf_choice.SetActive(true);
        Ballon_choice.SetActive(false);
         love_choice.SetActive(false);
         avacado_choice.SetActive(false);

               //no friends function
               leaf_character_img.SetActive(true);
                 balloon_character_img.SetActive(false);
                 love_character_img.SetActive(false);
                 avacado_character_img.SetActive(false);
    }
    public void unclock_function(){
        lock_function.SetActive(false);
    }
    //refine
    public void savebtn(){
        // nextpae.SetActive(true); // use it later(your choice page)
        choicepage_pg.SetActive(false);
        no_friends_found.SetActive(true);
        
      StartCoroutine(waitToShowFBScene4());
        IEnumerator waitToShowFBScene4(){
            	
                    
		yield return new WaitForSeconds (3);
      notification.Play();
            friends_found.SetActive(true); //this function
        }
        //this
        //refine
    } 
    public void hidebtn(){
        StartCoroutine(waitToShowFBScene3());
        IEnumerator waitToShowFBScene3(){
		yield return new WaitForSeconds (1);
		removehide.SetActive(false);
	}
    }
    public void okbtn(){
        no_friends_found.SetActive(false);
        okfucntion.SetActive(true);

    }
}
