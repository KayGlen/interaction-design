using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pagemanager : MonoBehaviour
{
    public GameObject gamepage;
    public GameObject character_selection;
///second page
    public GameObject avacado;
    public GameObject love;
    public GameObject balloons;
    public GameObject petals;
    public GameObject savebtnbtn;
    public GameObject emma_page;
    public GameObject hide;

    /////emma page
    public GameObject avacado_emma;
    public GameObject love_emma;
    public GameObject ballons_emma;
    public GameObject petals_emma;
    public GameObject speechbubles;
    public Animator speechbubble_anim;
    public Animator speechbubles_anim2;
    public GameObject speechbubles_02;
    public GameObject text_01;
    public GameObject text_02;
    public GameObject text_03;
    public GameObject text_04;
    public AudioSource messanger_typing;
    public AudioSource messanger_send;
    public GameObject nofriends_page;
    public GameObject friendspop_up;


    //audioSource
    public AudioSource pop_up_sound;


    // public GameObject readherstory;

        public GameObject buttontoappear;
        public GameObject storycompleted;

        // completed outcomes
        public GameObject completedoutcomes_page;
        public GameObject mainpage;

        public GameObject typewriter;
        public GameObject typewriter_02;
///no friends page
  public GameObject avacado_nofriends;
  public GameObject love_nofriends;
  public GameObject balloons_nofriends;
  public GameObject petals_nofriends;
  public AudioSource pop_up_sound_button;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //gamepage
    public void gamepagebtn(){
        gamepage.SetActive(false);
        character_selection.SetActive(true);
    }
    public void savebutton(){
        savebtnbtn.SetActive(false);
    }
    public void Avacadobtn(){
        avacado.SetActive(true);
        love.SetActive(false);
        balloons.SetActive(false);
        petals.SetActive(false);

        avacado_emma.SetActive(true);
        love_emma.SetActive(false);
        ballons_emma.SetActive(false);
        petals_emma.SetActive(false);
    }
    public void lovebtn(){
        avacado.SetActive(false);
        love.SetActive(true);
        balloons.SetActive(false);
        petals.SetActive(false);

        avacado_emma.SetActive(false);
        love_emma.SetActive(true);
        ballons_emma.SetActive(false);
        petals_emma.SetActive(false);
        
    }
      public void ballonsbtn(){
        avacado.SetActive(false);
        love.SetActive(false);
        balloons.SetActive(true);
        petals.SetActive(false);

         avacado_emma.SetActive(false);
        love_emma.SetActive(false);
        ballons_emma.SetActive(true);
        petals_emma.SetActive(false);
    }
     public void petalsbtn(){
        avacado.SetActive(false);
        love.SetActive(false);
        balloons.SetActive(false);
        petals.SetActive(true);

         avacado_emma.SetActive(false);
        love_emma.SetActive(false);
        ballons_emma.SetActive(false);
        petals_emma.SetActive(true);
    }
    public void savebtn_function(){
      
        nofriends_page.SetActive(true);
        character_selection.SetActive(false);
         StartCoroutine(waitToShowFBScene5());
        IEnumerator waitToShowFBScene5(){
            	
                  
		yield return new WaitForSeconds (2);
    friendspop_up.SetActive(true);
    pop_up_sound.Play();
        }
       
    // }
    // public void readherstory_btn_function(){
    //     readherstory.SetActive(true);
    //     emma_page.SetActive(false);
    // }
    
}
public void nofriendsbtn(){
    nofriends_page.SetActive(false);
  emma_page.SetActive(true);
      StartCoroutine(waitToShowFBScene4());
        IEnumerator waitToShowFBScene4(){
            	
                  
		yield return new WaitForSeconds (1);
          hide.SetActive(false);
        }
}
public void speechbubblesbtn(){
  messanger_typing.Play();
    speechbubles.SetActive(true);
    
      StartCoroutine(waitToShowFBScene5());
        IEnumerator waitToShowFBScene5(){
        yield return new WaitForSeconds (2);
       speechbubble_anim.Play("message_popup");
       messanger_typing.Stop();
       messanger_send.Play();
       text_01.SetActive(true);
          yield return new WaitForSeconds (1);
          messanger_typing.Play();
            speechbubble_anim.Play("message");
            yield return new WaitForSeconds (2);
             speechbubble_anim.Play("message_popup");
             text_02.SetActive(true);
              messanger_typing.Stop();
              messanger_send.Play();

               yield return new WaitForSeconds (1);
               speechbubble_anim.Play("message");
                 messanger_typing.Play();
               yield return new WaitForSeconds (2);
                messanger_typing.Stop();
                messanger_send.Play();
             speechbubble_anim.Play("message_popup");
             text_03.SetActive(true);
              yield return new WaitForSeconds (1);
               messanger_typing.Play();
               speechbubles_anim2.Play("message");
               speechbubles_02.SetActive(true);
               speechbubles.SetActive(false);
                   yield return new WaitForSeconds (2);
                     messanger_typing.Stop();
                   text_04.SetActive(true);
                   messanger_send.Play();
               speechbubles_anim2.Play("message_popup");
               yield return new WaitForSeconds (1);
               buttontoappear.SetActive(true);
               pop_up_sound_button.Play();
         
            
        }
        
}
public void buttonpopup(){
  storycompleted.SetActive(true);
}
public void completedbutton(){
  mainpage.SetActive(false);
  completedoutcomes_page.SetActive(true);
}
public void completedbutton_back(){
  mainpage.SetActive(true);
  completedoutcomes_page.SetActive(false);
  typewriter.SetActive(false);
  typewriter_02.SetActive(true);
}
public void destroyhide(){
  hide.SetActive(false);
}
public void nofriends_avacado_btn(){
  avacado_nofriends.SetActive(true);
    petals_nofriends.SetActive(false);
    balloons_nofriends.SetActive(false);
     love_nofriends.SetActive(false);
}
public void nofriends_petals_btn(){
  petals_nofriends.SetActive(true);
   balloons_nofriends.SetActive(false);
     love_nofriends.SetActive(false);
     avacado_nofriends.SetActive(false);
  
}
public void nofriends_ballons_btn(){
  balloons_nofriends.SetActive(true);
    petals_nofriends.SetActive(false);
       avacado_nofriends.SetActive(false);
     love_nofriends.SetActive(false);
}
public void nofriends_love_btn(){
  love_nofriends.SetActive(true);
  petals_nofriends.SetActive(false);
       avacado_nofriends.SetActive(false);
    balloons_nofriends.SetActive(false);
}



}
