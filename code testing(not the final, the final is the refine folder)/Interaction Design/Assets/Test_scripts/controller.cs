using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class controller : MonoBehaviour
{
    public Camera cam;
    public NavMeshAgent agent;
    public GameObject wayPoint;
     private Vector3 wayPointPos;
     private float speed = 6.0f;

    // Start is called before the first frame update
    void Start()
    {
        wayPoint = GameObject.Find("wayPoint");
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButton(0)){
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            

            if(Physics.Raycast(ray, out hit)){
                agent.SetDestination(hit.point);


               
            }
           
        }

        //follow player
              wayPointPos = new Vector3(wayPoint.transform.position.x, transform.position.y, wayPoint.transform.position.z);

            //   transform.position = Vector3.MoveTowards(transform.position, wayPointPos, speed * Time.deltaTime);

    }
}
