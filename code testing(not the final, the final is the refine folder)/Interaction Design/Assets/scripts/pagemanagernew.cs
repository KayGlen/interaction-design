using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pagemanagernew : MonoBehaviour
{
    [SerializeField]
        private GameObject[] characters;


    public void ChangeCharacters(int index)
    {
        for (int i = 0; i < characters.Length; i++)
        {
            characters[i].SetActive(false);
        }
            characters[index].SetActive(true);
    }
     public void Changebackcharacters(int index)
    {
        for (int i = 0; i < characters.Length; i++)
        {
            characters[i].SetActive(true);
        }
            characters[index].SetActive(false);
    }
}